# server and client for prusa camera

server and client for prusa camera

## watch on client using `cvlc`

run 

```bash
./watcher-cvlc
```

## installation on raspberry pi

### server

```bash
cp -v tcp-printer-video /root/tcp-printer-video
```

### service

run as `root`:

```bash
cp -v printer-video.service /etc/systemd/system/printer-video.service
systemctl enable printer-video.service
systemctl status printer-video.service
# optionally
systemctl start printer-video.service
```

